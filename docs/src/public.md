```@meta
CurrentModule = SimpleWorkflows
```

# Public API

```@docs
Workflow
run!
execute!
getstatus
liststatus
ispending
isrunning
isexited
issucceeded
isfailed
listpending
listrunning
listexited
listsucceeded
listfailed
```
